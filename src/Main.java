import java.io.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        RandomGenerator generator = new RandomGenerator();

        // setup distribution input
        for (int i = 1; i <= 12; i++) {
            generator.addDistributionValue(i, 830000);
        }

        generator.addDistributionValue(13, 1000);
        generator.addDistributionValue(14, 500);
        generator.addDistributionValue(15, 250);
        generator.addDistributionValue(16, 100);
        generator.addDistributionValue(17, 50);
        generator.addDistributionValue(18, 25);
        generator.addDistributionValue(19, 10);
        generator.addDistributionValue(20, 5);

        // generate output into file
        Writer writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("test.output"));
            ArrayList<Integer> list = new ArrayList<>();
            Integer i;
            for (i = 0; i < 997940; i++) {
                int rnd = generator.getRandomNumber();
                if (rnd == 20) {
                    list.add(i+1);
                }

                writer.write(rnd+"\n");
            }

            if ( list.size() > 0 ) {
                System.out.println("Total number of lines that have 20 is " + list.size());
                System.out.println(list.toString());
            } else {
                System.out.println("There are no lines that have value 20");
            }

        } catch (Exception e) {
            System.out.print(e);
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException ignored) {
            }
        }
    }
}
