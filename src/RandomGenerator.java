import java.util.HashMap;

public class RandomGenerator {
    private HashMap<Integer, Integer> distribution = new HashMap<>();
    private double distributionSum;

    /**
     * Add integer with distribution ratio
     *
     * @param value
     * @param distribution
     */
    public void addDistributionValue(int value, int distribution) {
        // Check if there is already a value in the distribution hash;
        // remove it so we can override with a new value
        if ( this.distribution.containsValue(value) ) {
            this.distributionSum -= this.distribution.get(value);
        }

        this.distribution.put(value, distribution);
        this.distributionSum += distribution;
    }

    /**
     * Get a random number within of given distribution set
     *
     * @return random integer
     */
    public Integer getRandomNumber() {
        double rand = Math.random();
        double ratio = 1.0f / this.distributionSum;
        double tempDist = 0;
        for (Integer i : distribution.keySet()) {
            tempDist += distribution.get(i);
            if (rand / ratio <= tempDist) {
                return i;
            }
        }
        return 0;
    }
}
